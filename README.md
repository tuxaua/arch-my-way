Set up Arch Linux, my way.

Characteristics
---------------

- Minimal set of packages
- Dotfiles managed with `chezmoi`
- Reproducible setup with `ansible`

Prerequisites
-------------

On the controller (where you clone this repo):

1. Install `ansible`.

2. Install AUR module `ansible-aur-git`.

3. Install `python`.

4. Create/reuse a ssh public/private key pair.

5. Configure `inventory` with your IP addresses or hostnames.

6. Adapt the `host` parameter in `system.yml`, `upgrade.yml`, and `dotfiles.yml` as necessary.
 
On the target:

1.  Install Arch Linux according to the [installation guide](https://wiki.archlinux.org/index.php/Installation_guide).

2.  Install `python`.

3.  Install `openssh`, allow `PubkeyAuthentication` in `/etc/ssh/sshd_config`,  and enable `sshd.service`.

4.  Create a user account named `ansible` with primary group `wheel`.
    If you want to reserve uid 1000 for the main user, set a different uid here.

5.  Install `sudo`. Edit `sudoers` to suppress passwords for user `ansible`.

6.  Append your public ssh key to `~ansible/.ssh/authorized_keys`.
    Make sure that the directory `.ssh` and file `authorized_keys` have permissions 700 and 600, respectively.

Usage
-----

### Change parameters

Edit `group_vars/all.yml`.

### Post installation setup

	ansible-playbook system.yml

### System upgrade

	ansible-playbook upgrade.yml

### Install dotfiles for main user

	ansible-playbook dotfiles.yml

If the `chezmoi` repository contains encrypted files, ensure that prior to running this command,
you have the established the following preconditions on the target machine:

- `gnupg` is installed. This is done when you run `system.yml`.
- `gnupg` is aware of your secret key. You need to import the secret key manually.
- `chezmoi` knows which key to use. Specify the recipient in `~/.config/chezmoi/chezmoi.toml`.
